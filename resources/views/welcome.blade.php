<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
  <title>Exam App</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <script src="{{ asset('js/app.js') }}" defer></script>
   <!-- Styles -->
</head>
<body>
  <div class="head">
    <h1 class="text-center">IP Address Search</h1>
  </div>
  <div class="frm">
    <div class="container-fluid">
    <form action="/" method="get">
          @csrf
          <div class="row m-2">
            <div class="col-sm-6"><label for="value1">City</label></div>
            <div class="col-sm-6"><label for="value1">Language</label></div>
          </div>
          <div class="row m-2">
            <div class="col-sm-6"><input type="text" name="q" id="" placeholder="Input City" value="Manila"></div>
            <div class="col-sm-6"><input type="text" name="language" id="" placeholder="en-us" value="en-us"></div>
          </div>
           <div class="btn-group">
           <button type="submit" class="mt-3 btn btn-info">Submit</button>
           </div>
        </form>
    </div>
  </div><!--FRM-->
  <div class="result">
    <div class="container-fluid">
    <table class="table table-hover">
        <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">LocalizedName</th>
      <th scope="col">PrimaryPostalCode</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    @isset($data)
      <th scope="row">1</th>
      <td>{{print_r($data["LocalizedName"])}}</td>
      <td>{{print_r($data["PrimaryPostalCode"])}}</td>
    </tr>
  </tbody>
  @endisset
</table>
    </div>
  </div>
  </div><!--Result-->
</body>
</html>